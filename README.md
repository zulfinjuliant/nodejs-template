# NodeJS Beginner Template
![gambar nodejs](http://i1087.photobucket.com/albums/j474/Zulfindra_Juliant/Screenshot-Dunia%20NodeJS%20-%20Chromium_zpsx9a18kfi.png)

## Teori
- Harus selalu di ingat, NodeJS itu single thread dengan event-callback
- NodeJS itu Monoglot Programming, yaitu semua coding di server side dan client side-nya berupa JavaScript

## Keterangan
- NodeJS + Jade Templating
- Bootstrap 3
- Awesome Animate CSS
- Font Awesome

## Referensi
- [Get NodeJS](https://nodejs.org)
- [Get Boostrap](https://getbootstrap.com)
- [Get Jade Templating](http://jade-lang.com)
- [Get Animate CSS](http://daneden.github.io/animate.css)
- [Get Font Awesome](http://fortawesome.github.io/Font-Awesome)

## Instalasi
- Pastikan PC kamu sudah terinstal NodeJS dan Npm 
(keywords Google : "Cara Install NodeJS di Windows / Ubuntu")
- Masuk ke direktori aplikasi/template lalu buka command line interface / terminal
- Ketik "node server.js", jika muncul notif log "OK" berarti bisa di jalankan
- Masuk ke browser, ketik "http://localhost:3000", taraaa muncul deh